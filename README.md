# Patch for Remote Code Execution vulnerability in the wp-reCaptcha plugin for Wordpress

Patch file here: https://git.insomnia247.nl/coolfire/wp-recaptcha-patch/blob/master/recaptcha.patch

The 'preg_replace' function allows for an executable modifier to be set on the regex. This causes any php code that ends up in this regex to be executed on the server. The wp-recaptcha plugin uses this functionality to url encode charaters in comments;
```js
$com = preg_replace('/([\\/\(\)\+\;\'])/e',
    '\'%\' . dechex(ord(\'$1\'))',
    $comment->comment_content);
```
Since php 7 the **/e** modifier is not allowed anymore and the usage of the **preg_replace_callback** function is required for this functionality anyway. It is also the simplest way I know of to fix the code execution issue in this plugin, so that is literally all this patch does. It replaces the usage of **preg_replace** with the **/e** modifier with the **preg_replace_callback** function.

## Why does this patch exist here?
I have notified the developers but looking at the versioning history, the plugin does not seem to be under very active development. In order for users to be able to protect themselves from this bug without having to resort to using another plugin, I have made this patch available.

## Side note
Instead of **dechex(ord())**, **urlencode()** could be used here, but this is outside the scope of this patch.

Update: Because I think it's a cleaner solution, I've also added a patch which uses the **urlencode** function rather than a regex replace. However, note that this does **change the behaviour** of the plugin slightly as it urlencodes a larger set of characters than the regex does. It shouldn't matter, but it's a largely untested modification.

Find it here: https://git.insomnia247.nl/coolfire/wp-recaptcha-patch/blob/master/recaptcha_urlencode.patch

## Original plugin home
https://wordpress.org/plugins/wp-recaptcha/

Update: As no fixes were being made for this plugin by the maintainers, the plugin has since been taken offline.